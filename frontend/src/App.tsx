import React, { useState } from 'react';
import './App.css';
import { PrimeReactProvider, PrimeReactContext } from 'primereact/api';
import { Menu } from 'primereact/menu';
import { Menubar } from 'primereact/menubar';
import { MenuItem } from 'primereact/menuitem';
import Login from './components/login/Login';
import ViewProcessInstances from './components/processes/ViewProcessInstances';
import ViewUsers from './components/roles/ViewUsers';
import { SessionUtils } from './utils/SessionUtils';
import { SessionInfo } from './utils/SessionInfo';

function App() {
    const [isUserLoggedIn, setIsUserLoggedIn] = useState<boolean>(false);
    const [activeComponent, setActiveComponent] = useState<any>(<ViewProcessInstances/>);
    const menuItems: MenuItem[] = [
        {
            label: 'Tareas',
            icon: 'pi pi-list-check',
            command: () => {setActiveComponent(<ViewProcessInstances/>)}
        },
        {
            label: 'Usuarios',
            icon: 'pi pi-user',
            command: () => {setActiveComponent(<ViewUsers/>)}
        }
    ];

    let menuEndItems: MenuItem[] = [
        { label: SessionUtils.getSessionInfo(SessionInfo.USER_FULL_NAME)!, icon: 'pi pi-user' }
    ];

    const menuEnd = (
        <Menu id="menu-end" model={menuEndItems} />
    );

    let appContent;

    if (isUserLoggedIn) {
        appContent = (
            <>
            <Menubar id="menubar" model={menuItems} end={menuEnd}/>
            {activeComponent}
            </>
        );
    } else {
        appContent = (
            <Login setIsUserLoggedIn={setIsUserLoggedIn} />
        );
    }

    return (
        <PrimeReactProvider>
            <div id="main">
                {appContent}
            </div>
        </PrimeReactProvider>
    );
}

export default App;
