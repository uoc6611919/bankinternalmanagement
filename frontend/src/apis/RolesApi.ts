import { CreateRoleDTO } from '../models/CreateRoleDTO';
import { RoleDTO } from '../models/RoleDTO';
import { FetchUtils } from '../utils/FetchUtils';

export class RolesApi {

    private static BASE_URL = '/roles';

    public static async getAll(): Promise<RoleDTO[]> {
        return await FetchUtils.fetchWithResponse<any, RoleDTO[]>(
            'GET',
            RolesApi.BASE_URL,
            null);
    }

    public static async create(createRoleDTO: CreateRoleDTO): Promise<RoleDTO> {
        return await FetchUtils.fetchWithResponse<CreateRoleDTO, RoleDTO>(
            'POST',
            RolesApi.BASE_URL,
            createRoleDTO);
    }

}
