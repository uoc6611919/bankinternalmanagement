import { LinkRolesDTO } from '../models/LinkRolesDTO';
import { UserDTO } from '../models/UserDTO';
import { FetchUtils } from '../utils/FetchUtils';

export class UsersApi {

    private static BASE_URL = '/users';

    public static async getAll(): Promise<UserDTO[]> {
        return await FetchUtils.fetchWithResponse<any, UserDTO[]>(
            'GET',
            UsersApi.BASE_URL,
            null);
    }

    public static async linkRoles(linkRolesDTO: LinkRolesDTO): Promise<UserDTO> {
        return await FetchUtils.fetchWithResponse<LinkRolesDTO, UserDTO>(
            'PUT',
            UsersApi.BASE_URL,
            linkRolesDTO);
    }

}
