import { PermissionDTO } from '../models/PermissionDTO';
import { FetchUtils } from '../utils/FetchUtils';

export class PermissionsApi {

    private static BASE_URL = '/permissions';

    public static async getAll(): Promise<PermissionDTO[]> {
        return await FetchUtils.fetchWithResponse<any, PermissionDTO[]>(
            'GET',
            PermissionsApi.BASE_URL,
            null);
    }

}
