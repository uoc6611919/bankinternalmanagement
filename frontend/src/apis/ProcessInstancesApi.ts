import { CreateProcessInstanceDTO } from '../models/CreateProcessInstanceDTO';
import { CompleteCurrentTaskDTO } from '../models/CompleteCurrentTaskDTO';
import { ProcessInstanceDTO } from '../models/ProcessInstanceDTO';
import { FetchUtils } from '../utils/FetchUtils';

export class ProcessInstancesApi {

    private static BASE_URL = '/processinstances';

    public static async getAll(): Promise<ProcessInstanceDTO[]> {
        return await FetchUtils.fetchWithResponse<any, ProcessInstanceDTO[]>(
            'GET',
            ProcessInstancesApi.BASE_URL,
            null);
    }

    public static async create(createProcessInstanceDTO: CreateProcessInstanceDTO): Promise<ProcessInstanceDTO> {
        return await FetchUtils.fetchWithResponse<CreateProcessInstanceDTO, ProcessInstanceDTO>(
            'POST',
            ProcessInstancesApi.BASE_URL,
            createProcessInstanceDTO);
    }

    public static async assign(processInstanceId: number): Promise<void> {
        await FetchUtils.fetchWithoutResponse<any>(
            'PUT',
            ProcessInstancesApi.BASE_URL + '/assign?processInstanceId=' + processInstanceId,
            null);
    }

    public static async completeCurrentTask(completeCurrentTaskDTO: CompleteCurrentTaskDTO): Promise<void> {
        await FetchUtils.fetchWithoutResponse<CompleteCurrentTaskDTO>(
            'PUT',
            ProcessInstancesApi.BASE_URL + '/completecurrenttask',
            completeCurrentTaskDTO);
    }

}
