import { ProcessDefinitionMinimizedDTO } from '../models/ProcessDefinitionMinimizedDTO';
import { FetchUtils } from '../utils/FetchUtils';

export class ProcessDefinitionsApi {

    private static BASE_URL = '/processdefinitions';

    public static async getAll(): Promise<ProcessDefinitionMinimizedDTO[]> {
        return await FetchUtils.fetchWithResponse<any, ProcessDefinitionMinimizedDTO[]>(
            'GET',
            ProcessDefinitionsApi.BASE_URL,
            null);
    }

    public static async getById(id: number): Promise<ProcessDefinitionMinimizedDTO> {
        return await FetchUtils.fetchWithResponse<any, ProcessDefinitionMinimizedDTO>(
            'GET',
            ProcessDefinitionsApi.BASE_URL + '/' + id,
            null);
    }

}
