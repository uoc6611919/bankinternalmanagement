import { LoginCredentialsDTO } from '../models/LoginCredentialsDTO';
import { UserDTO } from '../models/UserDTO';
import { FetchUtils } from '../utils/FetchUtils';

export class AuthApi {

    private static BASE_URL = '/auth';

    public static async login(loginCredentialsDTO: LoginCredentialsDTO): Promise<UserDTO> {
        return await FetchUtils.fetchWithResponse<LoginCredentialsDTO, UserDTO>(
            'POST',
            AuthApi.BASE_URL + '/login',
            loginCredentialsDTO);
    }

}
