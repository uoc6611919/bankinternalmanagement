import { SessionInfo } from './SessionInfo';
import { PermissionDTO } from '../models/PermissionDTO';
import { UserDTO } from '../models/UserDTO';

export class SessionUtils {

    public static setSessionInfoFromUser(user: UserDTO): void {
        localStorage.setItem(SessionInfo.AUTH_TOKEN, user.token);
        localStorage.setItem(SessionInfo.USER_NAME, user.username);
        localStorage.setItem(SessionInfo.USER_FULL_NAME, user.fullname);
        let userPermissions: string[] = SessionUtils.getPermissionsFromUser(user);
        localStorage.setItem(SessionInfo.USER_PERMISSIONS, JSON.stringify(userPermissions));
    }

    private static getPermissionsFromUser(user: UserDTO): string[] {
        let permissions: string[] = [];

        user.roles.forEach((role) => {
            role.permissions.forEach((permission: PermissionDTO) => {
                permissions.push(permission.name);
            });
        });

        permissions = [...new Set(permissions)];

        return permissions;
    }

    public static getSessionInfo(key: SessionInfo): string | null {
        return localStorage.getItem(key);
    }

}
