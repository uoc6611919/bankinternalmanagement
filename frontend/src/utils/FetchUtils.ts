import { SessionInfo } from './SessionInfo';
import { SessionUtils } from './SessionUtils';

export class FetchUtils {

    private static BASE_URL = 'http://localhost:8080';

    public static async fetchWithResponse<REQUEST_BODY, RESPONSE>(httpMethod: string, url: string, bodyObject: REQUEST_BODY): Promise<RESPONSE> {
        let fullUrl = FetchUtils.BASE_URL + url;
        let token = SessionUtils.getSessionInfo(SessionInfo.AUTH_TOKEN);

        let response = await fetch(
            fullUrl,
            {
                method: httpMethod,
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  ...token && {'Authorization': 'Bearer ' + token}
                },
                ...bodyObject && { body : JSON.stringify(bodyObject) }
            }
        );

        return response.json();
    }

    public static async fetchWithoutResponse<REQUEST_BODY>(httpMethod: string, url: string, bodyObject: REQUEST_BODY): Promise<void> {
        let fullUrl = FetchUtils.BASE_URL + url;
        let token = SessionUtils.getSessionInfo(SessionInfo.AUTH_TOKEN);

        await fetch(
            fullUrl,
            {
                method: httpMethod,
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  ...token && {'Authorization': 'Bearer ' + token}
                },
                ...bodyObject && { body : JSON.stringify(bodyObject) }
            }
        );
    }

}
