import './ViewProcessInstance.css';
import React, { useState, useEffect } from 'react';
import { Button } from 'primereact/button';
import { MenuItem } from 'primereact/menuitem';
import { Panel } from 'primereact/panel';
import { Steps } from 'primereact/steps';
import { ProcessDefinitionsApi } from '../../apis/ProcessDefinitionsApi';
import { ProcessInstancesApi } from '../../apis/ProcessInstancesApi';
import { CompleteCurrentTaskDTO } from '../../models/CompleteCurrentTaskDTO';
import { ProcessDefinitionMinimizedDTO } from '../../models/ProcessDefinitionMinimizedDTO';
import { ProcessInstanceDTO } from '../../models/ProcessInstanceDTO';

export default function ViewProcessInstance({processInstance, setVisible, reloadProcessInstances}: {
            processInstance: ProcessInstanceDTO,
            setVisible: React.Dispatch<React.SetStateAction<boolean>>,
            reloadProcessInstances: ()=>void
    }) {
    const [steps, setSteps] = useState<MenuItem[]>([]);


    const buildMenuItems = (processDefinition: ProcessDefinitionMinimizedDTO) => {
        const menuItems: MenuItem[] = processDefinition.taskDefinitions
            .sort((td1, td2) => (td1.order-td2.order))
            .map(td => {
                const menuItem: MenuItem = {
                    label: td.name
                };
                return menuItem;
            });

        setSteps(menuItems);
    };

    const loadProcessDefinition = () => {
        ProcessDefinitionsApi.getById(processInstance.processDefinition.id)
        .then((data) => buildMenuItems(data))
        .catch((err) => console.log(err));
    };

    useEffect(() => loadProcessDefinition(), []);

    const onComplete = (action: string) => {
        const completeCurrentTaskDTO: CompleteCurrentTaskDTO = {
            processInstanceId: processInstance.id,
            action: action
        };

        ProcessInstancesApi.completeCurrentTask(completeCurrentTaskDTO)
        .then(() => {
            setVisible(false);
            reloadProcessInstances();
        })
        .catch((err) => console.log(err));
    };

    return (
        <Panel id="view-process-instance-panel"
            header={'PROCESO: ' + processInstance.id + ' - ' + processInstance.processDefinition.name + ' - ' + processInstance.title}>
            <div id="view-process-instance-container">
                <Steps id="view-process-instance-steps" model={steps} activeIndex={processInstance.currentTask.order - 1}/>
                <label id="view-process-instance-label">¿Cómo desea finalizar la tarea?</label>
                <div id="view-process-instance-buttons">
                    <Button label="Aceptar" severity="success" icon="pi pi-check" onClick={() => onComplete('COMPLETE')}/>
                    <Button label="Rechazar" severity="danger" icon="pi pi-times" onClick={() => onComplete('CANCEL')}/>
                </div>
            </div>
        </Panel>
    );
}
