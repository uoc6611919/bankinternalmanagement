import React, { useState, useEffect } from 'react';
import './ViewProcessInstances.css';
import { Button } from 'primereact/button';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { Dialog } from 'primereact/dialog';
import { Panel } from 'primereact/panel';
import { Tag } from 'primereact/tag';
import { ProcessInstancesApi } from '../../apis/ProcessInstancesApi';
import { ProcessInstanceDTO } from '../../models/ProcessInstanceDTO';
import CreateProcessInstance from './CreateProcessInstance';
import ViewProcessInstance from './ViewProcessInstance';
import { SessionUtils } from '../../utils/SessionUtils';
import { SessionInfo } from '../../utils/SessionInfo';

export default function ViewProcessInstances() {
    const [processInstances, setProcessInstances] = useState<ProcessInstanceDTO[]>([]);
    const [createProcessInstanceVisible, setCreateProcessInstanceVisible] = useState<boolean>(false);
    const [viewProcessInstanceVisible, setViewProcessInstanceVisible] = useState<boolean>(false);
    const [selectedProcessInstance, setSelectedProcessInstance] = useState<ProcessInstanceDTO | null>(null);

    const loadProcessInstances = () => {
        ProcessInstancesApi.getAll()
        .then((data) => setProcessInstances(data))
        .catch((err) => console.log(err));
    }

    useEffect(() => loadProcessInstances(), []);

    const creationDateColumnTemplate = (processInstance: ProcessInstanceDTO) => {
        return processInstance.creationDate.toLocaleString().replace('T', ' ');
    };

    const statusColumnTemplate = (processInstance: ProcessInstanceDTO) => {
        return <Tag value={processInstance.status} severity={getSeverity(processInstance)}></Tag>;
    };

    const getSeverity = (processInstance: ProcessInstanceDTO) => {
        switch (processInstance.status) {
            case 'IN_PROGRESS':
                return 'info';
            case 'COMPLETED':
                return 'success';
            case 'CANCELLED':
                return 'danger';
            default:
                return null;
        }
    };

    const assignColumnTemplate = (processInstance: ProcessInstanceDTO) => {
        return <Button label="Asignar" onClick={() => onAssign(processInstance)}/>;
    };

    const onAssign = (processInstance: ProcessInstanceDTO) => {
        ProcessInstancesApi.assign(processInstance.id)
        .then(() => loadProcessInstances())
        .catch((err) => console.log(err));
    };

    const openColumnTemplate = (processInstance: ProcessInstanceDTO) => {
        return <Button label="Abrir" onClick={() => onOpen(processInstance)} disabled={isOpenDisabled(processInstance)}/>;
    };

    const onOpen = (processInstance: ProcessInstanceDTO) => {
        setSelectedProcessInstance(processInstance);
        setViewProcessInstanceVisible(true);
    };

    const userHasRequiredPermission = (processInstance: ProcessInstanceDTO): boolean => {
        let requiredPermission = processInstance.currentTask.requiredPermission.name;
        let userPermissions = SessionUtils.getSessionInfo(SessionInfo.USER_PERMISSIONS);
        return userPermissions!.includes(requiredPermission);
    };

    const isOpenDisabled = (processInstance: ProcessInstanceDTO) => {
        const processInstanceStatus: string = processInstance.status;
        return processInstanceStatus == 'COMPLETED'
                || processInstanceStatus == 'CANCELLED'
                || !userHasRequiredPermission(processInstance);
    };

    return (
        <Panel header="Tareas">
            <div id="view-process-instances-container">
                <div id="view-process-instances-buttons">
                    <Button label="Crear proceso" icon="pi pi-plus" onClick={() => setCreateProcessInstanceVisible(true)}/>
                </div>
                <DataTable id="view-process-instances-table" value={processInstances} tableStyle={{ minWidth: '50rem' }} >
                    <Column field="id" header="ID"></Column>
                    <Column field="processDefinition.name" header="Nombre proceso"></Column>
                    <Column field="title" header="Título proceso"></Column>
                    <Column field="currentTask.name" header="Tarea actual"></Column>
                    <Column body={creationDateColumnTemplate} header="Fecha creación"></Column>
                    <Column body={statusColumnTemplate} header="Estado"></Column>
                    <Column field="userAssigned.fullname" header="Asignado"></Column>
                    <Column body={assignColumnTemplate}></Column>
                    <Column body={openColumnTemplate}></Column>
                </DataTable>
                <Dialog visible={createProcessInstanceVisible} onHide={() => setCreateProcessInstanceVisible(false)} style={{ width: '65vw' }}>
                    <CreateProcessInstance setVisible={setCreateProcessInstanceVisible} reloadProcessInstances={loadProcessInstances}/>
                </Dialog>
                <Dialog visible={viewProcessInstanceVisible} onHide={() => setViewProcessInstanceVisible(false)} style={{ width: '65vw' }}>
                    <ViewProcessInstance processInstance={selectedProcessInstance!} setVisible={setViewProcessInstanceVisible} reloadProcessInstances={loadProcessInstances}/>
                </Dialog>
            </div>
        </Panel>
    );
}
