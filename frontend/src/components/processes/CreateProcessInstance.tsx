import React, { useState, useEffect, useRef } from 'react';
import './CreateProcessInstance.css';
import { Button } from 'primereact/button';
import { Dropdown, DropdownChangeEvent } from 'primereact/dropdown';
import { InputText } from 'primereact/inputtext';
import { Panel } from 'primereact/panel';
import { ProcessDefinitionsApi } from '../../apis/ProcessDefinitionsApi';
import { ProcessInstancesApi } from '../../apis/ProcessInstancesApi';
import { CreateProcessInstanceDTO } from '../../models/CreateProcessInstanceDTO';
import { ProcessDefinitionMinimizedDTO } from '../../models/ProcessDefinitionMinimizedDTO';

export default function CreateProcessInstance({setVisible, reloadProcessInstances}: {
            setVisible: React.Dispatch<React.SetStateAction<boolean>>,
            reloadProcessInstances: ()=>void
    }) {
    const [title, setTitle] = useState<string>('');
    const [selectedProcessDefinition, setSelectedProcessDefinition] = useState<ProcessDefinitionMinimizedDTO | null>(null);
    const [processDefinitions, setProcessDefinitions] = useState<ProcessDefinitionMinimizedDTO[]>([]);

    useEffect(() => {
        ProcessDefinitionsApi.getAll()
        .then((data) => {
            setProcessDefinitions(data);
            setSelectedProcessDefinition(data[0])
        })
        .catch((err) => console.log(err));
    }, []);

    const onSave = () => {
        const createProcessInstanceDTO: CreateProcessInstanceDTO = {
            title: title,
            processDefinitionId: selectedProcessDefinition!.id
        };

        ProcessInstancesApi.create(createProcessInstanceDTO)
        .then(() => {
            setVisible(false);
            reloadProcessInstances();
        });
    };

    return (
        <Panel header="Crear proceso">
            <div id="create-process-container">
                <label id="create-process-label-title">Título</label>
                <InputText id="create-process-title" value={title} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setTitle(e.target.value)} />
                <label id="create-process-label-process">Proceso</label>
                <Dropdown id="create-process-process" value={selectedProcessDefinition} onChange={(e: DropdownChangeEvent) => setSelectedProcessDefinition(e.value)} options={processDefinitions}
                    optionLabel="name" />
                <div id="create-process-buttons">
                    <Button label="Guardar" onClick={onSave}/>
                </div>
            </div>
        </Panel>
    );
}
