import React, { useState, useRef } from 'react';
import './Login.css';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Panel } from 'primereact/panel';
import { Password } from 'primereact/password';
import { Toast } from 'primereact/toast';
import { AuthApi } from '../../apis/AuthApi';
import { LoginCredentialsDTO } from '../../models/LoginCredentialsDTO';
import { SessionUtils } from '../../utils/SessionUtils';

export default function Login({setIsUserLoggedIn}: {
    setIsUserLoggedIn: React.Dispatch<React.SetStateAction<boolean>>
    }) {
    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const toast = useRef<Toast>(null);

    const onLogin = () => {
        const loginCredentialsDTO: LoginCredentialsDTO = {
            username: username,
            password: password
        };

        AuthApi.login(loginCredentialsDTO)
        .then((user) => {
            SessionUtils.setSessionInfoFromUser(user);
            setIsUserLoggedIn(true);
        })
        .catch((error) => {
            toast.current?.show({
                severity: 'error',
                summary: 'Error',
                detail: 'Usuario o contraseña incorrectos.'
            });

            console.log(error);
        });
    };

    return (
        <Panel id="login-panel" header="Iniciar sesión">
            <div id="login-container">
                <label id="login-label-username">Usuario</label>
                <InputText id="login-username" value={username} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setUsername(e.target.value)} />
                <label id="login-label-password">Constraseña</label>
                <Password id="login-password" value={password} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.target.value)} />
                <div id="login-buttons">
                    <Button label="Iniciar" onClick={onLogin}/>
                </div>
                <Toast ref={toast}/>
            </div>
        </Panel>
    );
}
