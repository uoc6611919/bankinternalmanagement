import React, { useState, useEffect, useRef } from 'react';
import './ViewUsers.css';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import { Dialog } from 'primereact/dialog';
import { MenuItem } from 'primereact/menuitem';
import { Panel } from 'primereact/panel';
import { SplitButton } from 'primereact/splitbutton';
import { Toast } from 'primereact/toast';
import { UsersApi } from '../../apis/UsersApi';
import { UserDTO } from '../../models/UserDTO';
import CreateRole from './CreateRole';
import LinkRoles from './LinkRoles';

export default function ViewUsers() {
    const [users, setUsers] = useState<UserDTO[]>([]);
    const [selectedUser, setSelectedUser] = useState<UserDTO | null>(null);
    const [createRoleVisible, setCreateRoleVisible] = useState<boolean>(false);
    const [linkRolesVisible, setLinkRolesVisible] = useState<boolean>(false);
    const toast = useRef<Toast>(null);
    const splitButtonItems: MenuItem[] = [
        {
            label: 'Crear',
            icon: 'pi pi-plus',
            command: () => {
                setCreateRoleVisible(true);
            }
        },
        {
            label: 'Vincular',
            icon: 'pi pi-angle-double-right',
            command: () => {
                if (selectedUser != null) {
                    setLinkRolesVisible(true);
                } else {
                    toast.current?.show({
                        severity: 'info',
                        summary: 'Info',
                        detail: 'Selecciona un usuario para poder vincularle roles.'
                    });
                }
            }
        }
    ];

    const loadUsers = () => {
        UsersApi.getAll()
        .then((data) => setUsers(data))
        .catch((err) => console.log(err));
    }

    useEffect(() => loadUsers(), []);

    const rolesColumnTemplate = (user: UserDTO) => {
        let rolesAndPermissions: any[] = [];

        user.roles.forEach((role) => {
            let permissions = role.permissions.map(p => <li key={p.id}>{p.name}</li>);
            let rolesAndPermission = <p><b>{role.name}: </b>{permissions}</p>;
            rolesAndPermissions.push(rolesAndPermission);
        });

        return rolesAndPermissions;
    };

    return (
        <Panel header="Usuarios">
            <div id="view-users-container" >
                <div id="view-users-buttons">
                    <Toast ref={toast}/>
                    <SplitButton label="Roles" icon="pi pi-plus" model={splitButtonItems} />
                </div>
                <DataTable id="view-users-table" value={users} selection={selectedUser!} onSelectionChange={(e) => setSelectedUser(e.value)}
                        dataKey="id" tableStyle={{ minWidth: '50rem' }} >
                    <Column selectionMode="single" headerStyle={{ width: '3rem' }}></Column>
                    <Column field="username" header="Usuario"></Column>
                    <Column field="fullname" header="Nombre"></Column>
                    <Column field="email" header="Email"></Column>
                    <Column field="roles" header="Roles y permisos" body={rolesColumnTemplate}></Column>
                </DataTable>
                <Dialog visible={createRoleVisible} onHide={() => setCreateRoleVisible(false)} style={{ width: '65vw' }}>
                    <CreateRole setVisible={setCreateRoleVisible}/>
                </Dialog>
                <Dialog visible={linkRolesVisible} onHide={() => setLinkRolesVisible(false)} style={{ width: '65vw' }}>
                    <LinkRoles user={selectedUser!} setVisible={setLinkRolesVisible} reloadUsers={loadUsers}/>
                </Dialog>
            </div>
        </Panel>
    );
}
