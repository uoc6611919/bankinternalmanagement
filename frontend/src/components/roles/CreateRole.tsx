import React, { useState, useEffect } from 'react';
import './CreateRole.css';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Panel } from 'primereact/panel';
import { PickList } from 'primereact/picklist';
import { PermissionsApi } from '../../apis/PermissionsApi';
import { RolesApi } from '../../apis/RolesApi';
import { PermissionDTO } from '../../models/PermissionDTO';
import { CreateRoleDTO } from '../../models/CreateRoleDTO';

export default function CreateRole({setVisible}: {setVisible: React.Dispatch<React.SetStateAction<boolean>>}) {
    const [rolName, setRolName] = useState<string>('');
    const [available, setAvailable] = useState<PermissionDTO[]>([]);
    const [selected, setSelected] = useState<PermissionDTO[]>([]);

    useEffect(() => {
        PermissionsApi.getAll()
        .then((data) => setAvailable(data))
        .catch((err) => console.log(err));
    }, []);

    const onChangePickList = (event: { source: React.SetStateAction<PermissionDTO[]>; target: React.SetStateAction<PermissionDTO[]>; }) => {
        setAvailable(event.source);
        setSelected(event.target);
    };

    const itemTemplate = (item: PermissionDTO) => {
        return <div>{item.name}</div>
    };

    const onSave = () => {
        const permissionIds = selected.map(p => p.id);

        const createRoleDTO: CreateRoleDTO = {
            name: rolName,
            permissionIds: permissionIds
        };

        RolesApi.create(createRoleDTO)
        .then(() => setVisible(false));
    };

    return (
        <Panel header="Crear rol">
            <div id="create-role-container">
                <label id="create-role-label-name">Nombre</label>
                <InputText id="create-role-name" value={rolName} onChange={(e: React.ChangeEvent<HTMLInputElement>) => setRolName(e.target.value)} />
                <label id="create-role-label-permissions">Permisos</label>
                <PickList id="create-role-permissions" dataKey="id" source={available} target={selected} onChange={onChangePickList} itemTemplate={itemTemplate} breakpoint="1280px"
                    sourceHeader="Disponibles" targetHeader="Seleccionados" sourceStyle={{ height: '24rem' }} targetStyle={{ height: '24rem' }} />
                <div id="create-role-buttons">
                    <Button label="Guardar" onClick={onSave}/>
                </div>
            </div>
        </Panel>
    );
}
