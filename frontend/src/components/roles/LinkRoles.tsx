import React, { useState, useEffect } from 'react';
import './LinkRoles.css';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Panel } from 'primereact/panel';
import { PickList } from 'primereact/picklist';
import { RolesApi } from '../../apis/RolesApi';
import { UsersApi } from '../../apis/UsersApi';
import { LinkRolesDTO } from '../../models/LinkRolesDTO';
import { RoleDTO } from '../../models/RoleDTO';
import { UserDTO } from '../../models/UserDTO';

export default function LinkRoles({user, setVisible, reloadUsers}: {
        user: UserDTO,
        setVisible: React.Dispatch<React.SetStateAction<boolean>>,
        reloadUsers: ()=>void
    }) {
    const [available, setAvailable] = useState<RoleDTO[]>([]);
    const [selected, setSelected] = useState<RoleDTO[]>([]);

    useEffect(() => {
        RolesApi.getAll()
        .then((allRoles: RoleDTO[]) => {
            const selectedRoles: RoleDTO[] = user.roles;
            const availableRoles: RoleDTO[] = allRoles.filter(role => !selectedRoles.map(sr => sr.id).includes(role.id));

            setSelected(selectedRoles);
            setAvailable(availableRoles);
        })
        .catch((err) => console.log(err));
    }, []);

    const onChangePickList = (event: { source: React.SetStateAction<RoleDTO[]>; target: React.SetStateAction<RoleDTO[]>; }) => {
        setAvailable(event.source);
        setSelected(event.target);
    };

    const itemTemplate = (item: RoleDTO) => {
        return <div>{item.name}</div>
    };

    const onSave = () => {
        const roleIds = selected.map(r => r.id);

        const linkRolesDTO: LinkRolesDTO = {
            userId: user.id,
            roleIds: roleIds
        };

        UsersApi.linkRoles(linkRolesDTO)
        .then(() => {
            setVisible(false);
            reloadUsers();
        });
    };

    return (
        <Panel header="Vincular roles">
            <div id="link-roles-container">
                <label id="link-roles-label-user">Nombre usuario</label>
                <InputText id="link-roles-user" value={user.fullname} disabled />
                <label id="link-roles-label-roles">Roles</label>
                <PickList id="link-roles-roles" dataKey="id" source={available} target={selected} onChange={onChangePickList} itemTemplate={itemTemplate} breakpoint="1280px"
                    sourceHeader="Disponibles" targetHeader="Seleccionados" sourceStyle={{ height: '24rem' }} targetStyle={{ height: '24rem' }} />
                <div id="link-roles-buttons">
                    <Button label="Guardar" onClick={onSave}/>
                </div>
            </div>
        </Panel>
    );
}
