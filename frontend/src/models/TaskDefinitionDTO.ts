import { PermissionDTO } from './PermissionDTO';
import { ProcessDefinitionDTO } from './ProcessDefinitionDTO';

export interface TaskDefinitionDTO {
    id: number;
    name: string;
    order: number;
    requiredPermission: PermissionDTO;
    processDefinition: ProcessDefinitionDTO;
}
