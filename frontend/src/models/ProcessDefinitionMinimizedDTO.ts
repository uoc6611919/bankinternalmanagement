import { TaskDefinitionMinimizedDTO } from './TaskDefinitionMinimizedDTO';

export interface ProcessDefinitionMinimizedDTO {
    id: number;
    name: string;
    taskDefinitions: TaskDefinitionMinimizedDTO[];
}
