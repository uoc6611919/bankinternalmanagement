export interface CompleteCurrentTaskDTO {
    processInstanceId: number;
    action: string;
}
