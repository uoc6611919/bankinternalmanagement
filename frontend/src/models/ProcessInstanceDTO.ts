import { ProcessDefinitionDTO } from './ProcessDefinitionDTO';
import { TaskDefinitionDTO } from './TaskDefinitionDTO';
import { UserDTO } from './UserDTO';

export interface ProcessInstanceDTO {
    id: number;
    title: string;
    status: string;
    creationDate: Date;
    userAssigned: UserDTO;
    processDefinition: ProcessDefinitionDTO;
    currentTask: TaskDefinitionDTO;
}
