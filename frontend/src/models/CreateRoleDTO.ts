export interface CreateRoleDTO {
    name: string;
    permissionIds: number[];
}
