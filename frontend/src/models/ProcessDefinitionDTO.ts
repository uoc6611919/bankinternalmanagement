export interface ProcessDefinitionDTO {
    id: number;
    name: string;
}
