import { PermissionDTO } from './PermissionDTO';

export interface TaskDefinitionMinimizedDTO {
    id: number;
    name: string;
    order: number;
    requiredPermission: PermissionDTO;
}
