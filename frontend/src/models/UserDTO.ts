import { RoleDTO } from './RoleDTO';

export interface UserDTO {
    id: number;
    username: string;
    fullname: string;
    email: string;
    token: string;
    roles: RoleDTO[];
}
