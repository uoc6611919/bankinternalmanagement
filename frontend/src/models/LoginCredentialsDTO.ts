export interface LoginCredentialsDTO {
    username: string;
    password: string;
}
