export interface LinkRolesDTO {
    userId: number;
    roleIds: number[];
}
