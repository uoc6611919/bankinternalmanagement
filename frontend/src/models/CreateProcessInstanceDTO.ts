export interface CreateProcessInstanceDTO {
    title: string;
    processDefinitionId: number;
}
