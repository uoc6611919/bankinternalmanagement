# BankInternalManagement

## Run the application

Prerequisites: you'll need docker running on your computer.

Steps:

1. Download the project
2. Locate yourself in the root directory, that is: "/BankInternalManagement"
3. Start the MariaDB database by executing the following command: “docker compose up -d db”
4. Connect to the database with any SQL client using the following credentials:
- host: localhost
- database: bankinternalmanagement
- username: root
- password: root
5. Execute the following SQL statements:
- CREATE DATABASE bankinternalmanagement;
- CREATE USER 'user1'@localhost IDENTIFIED BY 'user1';
- GRANT ALL PRIVILEGES ON bankinternalmanagement.* TO 'user1'@localhost;
6. Start the backend and the frontend by executing the following command: “docker compose up -d”
7. Now the application should be available at: http://localhost:3000/
8. The credentials are:
- User: calbiol
- Password: 1234
