package com.raulgoklani.bankinternalmanagement.models;

public enum ProcessInstanceStatus {
    IN_PROGRESS,
    COMPLETED,
    CANCELLED
}
