package com.raulgoklani.bankinternalmanagement.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "process_definition")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProcessDefinition {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "processDefinition")
    private Set<TaskDefinition> taskDefinitions;

}
