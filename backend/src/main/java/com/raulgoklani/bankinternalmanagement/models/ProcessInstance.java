package com.raulgoklani.bankinternalmanagement.models;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Table(name = "process_instance")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProcessInstance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "title")
    private String title;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ProcessInstanceStatus status;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User userAssigned;

    @ManyToOne
    @JoinColumn(name="process_definition_id")
    private ProcessDefinition processDefinition;

    @ManyToOne
    @JoinColumn(name="task_definition_id")
    private TaskDefinition currentTask;

}
