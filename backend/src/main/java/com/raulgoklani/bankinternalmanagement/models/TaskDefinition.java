package com.raulgoklani.bankinternalmanagement.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "task_definition")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskDefinition {

    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "order")
    private Long order;

    @ManyToOne
    @JoinColumn(name="permission_id")
    private Permission requiredPermission;

    @ManyToOne
    @JoinColumn(name="process_definition_id")
    private ProcessDefinition processDefinition;

}
