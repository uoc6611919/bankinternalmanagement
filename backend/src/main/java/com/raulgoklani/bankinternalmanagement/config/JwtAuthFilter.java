package com.raulgoklani.bankinternalmanagement.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@AllArgsConstructor
public class JwtAuthFilter extends OncePerRequestFilter {

    private AuthProvider authProvider;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String requestHeaderAuth = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (isBearerToken(requestHeaderAuth)) {
            String token = getToken(requestHeaderAuth);

            try {
                Authentication tokenAuthentication = authProvider.validateToken(token);
                SecurityContextHolder.getContext().setAuthentication(tokenAuthentication);
            } catch (Exception ex) {
                SecurityContextHolder.clearContext();
            }
        }

        filterChain.doFilter(request, response);

    }

    private boolean isBearerToken(String requestHeaderAuth) {
        if (requestHeaderAuth != null) {
            String[] requestHeaderAuthParts = requestHeaderAuth.split(" ");

            if (requestHeaderAuthParts.length == 2) {
                String authType = requestHeaderAuthParts[0];
                return "Bearer".equals(authType);
            }
        }

        return false;
    }

    private String getToken(String requestHeaderAuth) {
        return requestHeaderAuth.split(" ")[1];
    }

}
