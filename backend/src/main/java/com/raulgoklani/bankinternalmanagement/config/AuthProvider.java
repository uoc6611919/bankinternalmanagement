package com.raulgoklani.bankinternalmanagement.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.raulgoklani.bankinternalmanagement.models.User;
import com.raulgoklani.bankinternalmanagement.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Date;
import java.util.Optional;

@Component
public class AuthProvider {

    @Value("security.jwt.token.secret-key")
    private String secretKey;

    @Autowired
    private UserRepository userRepository;

    public String createToken(String username) {
        Date issuedDate = new Date();
        Date expiryDate = new Date(issuedDate.getTime() + 3_600_000);

        return JWT.create()
                .withIssuer(username)
                .withIssuedAt(issuedDate)
                .withExpiresAt(expiryDate)
                .sign(Algorithm.HMAC256(secretKey));
    }

    public Authentication validateToken(String token) {
        JWTVerifier verifier = JWT
                .require(Algorithm.HMAC256(secretKey))
                .build();

        DecodedJWT decodedToken = verifier.verify(token);

        String username = decodedToken.getIssuer();

        Optional<User> userOptional = userRepository.findByUsername(username);
        User user = userOptional.orElse(null);

        return new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList());
    }

}
