package com.raulgoklani.bankinternalmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankinternalmanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankinternalmanagementApplication.class, args);
	}

}
