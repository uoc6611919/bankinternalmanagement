package com.raulgoklani.bankinternalmanagement.controllers;

import com.raulgoklani.bankinternalmanagement.dtos.CreateRoleDTO;
import com.raulgoklani.bankinternalmanagement.dtos.LinkRolesDTO;
import com.raulgoklani.bankinternalmanagement.dtos.UserDTO;
import com.raulgoklani.bankinternalmanagement.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> getAll() {
        List<UserDTO> allUsers = userService.getAll();
        return new ResponseEntity<>(allUsers, HttpStatus.OK);
    }

    @PutMapping("/users")
    public ResponseEntity<UserDTO> linkRoles(@RequestBody LinkRolesDTO linkRolesDTO) {
        UserDTO user = userService.linkRoles(linkRolesDTO);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

}
