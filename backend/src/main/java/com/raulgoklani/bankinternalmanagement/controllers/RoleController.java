package com.raulgoklani.bankinternalmanagement.controllers;

import com.raulgoklani.bankinternalmanagement.dtos.CreateRoleDTO;
import com.raulgoklani.bankinternalmanagement.dtos.RoleDTO;
import com.raulgoklani.bankinternalmanagement.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/roles")
    public ResponseEntity<List<RoleDTO>> getAll() {
        List<RoleDTO> allRoles = roleService.getAll();
        return new ResponseEntity<>(allRoles, HttpStatus.OK);
    }

    @PostMapping("/roles")
    public ResponseEntity<RoleDTO> create(@RequestBody CreateRoleDTO createRoleDTO) {
        RoleDTO roleDTO = roleService.create(createRoleDTO);
        return new ResponseEntity<>(roleDTO, HttpStatus.OK);
    }

}
