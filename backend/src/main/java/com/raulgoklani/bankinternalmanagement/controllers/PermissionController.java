package com.raulgoklani.bankinternalmanagement.controllers;

import com.raulgoklani.bankinternalmanagement.dtos.PermissionDTO;
import com.raulgoklani.bankinternalmanagement.services.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PermissionController {

    @Autowired
    private PermissionService permissionService;

    @GetMapping("/permissions")
    public ResponseEntity<List<PermissionDTO>> getAll() {
        List<PermissionDTO> allPermissions = permissionService.getAll();
        return new ResponseEntity<>(allPermissions, HttpStatus.OK);
    }

}
