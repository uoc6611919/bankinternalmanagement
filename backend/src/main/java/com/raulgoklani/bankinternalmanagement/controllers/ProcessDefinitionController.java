package com.raulgoklani.bankinternalmanagement.controllers;

import com.raulgoklani.bankinternalmanagement.dtos.ProcessDefinitionMinimizedDTO;
import com.raulgoklani.bankinternalmanagement.services.ProcessDefinitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProcessDefinitionController {

    @Autowired
    private ProcessDefinitionService processDefinitionService;

    @GetMapping("/processdefinitions")
    public ResponseEntity<List<ProcessDefinitionMinimizedDTO>> getAll() {
        List<ProcessDefinitionMinimizedDTO> allProcessDefinitions = processDefinitionService.getAll();
        return new ResponseEntity<>(allProcessDefinitions, HttpStatus.OK);
    }

    @GetMapping("/processdefinitions/{id}")
    public ResponseEntity<ProcessDefinitionMinimizedDTO> getById(@PathVariable Long id) {
        ProcessDefinitionMinimizedDTO processDefinition = processDefinitionService.getById(id);
        return new ResponseEntity<>(processDefinition, HttpStatus.OK);
    }

}
