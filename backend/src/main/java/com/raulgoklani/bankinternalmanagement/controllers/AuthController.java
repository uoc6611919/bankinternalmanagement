package com.raulgoklani.bankinternalmanagement.controllers;

import com.raulgoklani.bankinternalmanagement.dtos.LoginCredentialsDTO;
import com.raulgoklani.bankinternalmanagement.dtos.UserDTO;
import com.raulgoklani.bankinternalmanagement.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthController {

    @Autowired
    private UserService userService;

    @PostMapping("/auth/login")
    public ResponseEntity<UserDTO> login(@RequestBody LoginCredentialsDTO loginCredentialsDTO) {
        UserDTO userDTO = userService.login(loginCredentialsDTO);
        return new ResponseEntity<>(userDTO, HttpStatus.OK);
    }

}
