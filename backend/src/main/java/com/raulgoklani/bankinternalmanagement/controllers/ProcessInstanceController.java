package com.raulgoklani.bankinternalmanagement.controllers;

import com.raulgoklani.bankinternalmanagement.dtos.*;
import com.raulgoklani.bankinternalmanagement.services.ProcessInstanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProcessInstanceController {

    @Autowired
    private ProcessInstanceService processInstanceService;

    @GetMapping("/processinstances")
    public ResponseEntity<List<ProcessInstanceDTO>> getAll() {
        List<ProcessInstanceDTO> allProcessInstances = processInstanceService.getAll();
        return new ResponseEntity<>(allProcessInstances, HttpStatus.OK);
    }

    @PostMapping("/processinstances")
    public ResponseEntity<ProcessInstanceDTO> create(@RequestBody CreateProcessInstanceDTO createProcessInstanceDTO) {
        ProcessInstanceDTO processInstanceDTO = processInstanceService.create(createProcessInstanceDTO);
        return new ResponseEntity<>(processInstanceDTO, HttpStatus.OK);
    }

    @PutMapping("/processinstances/assign")
    public ResponseEntity assign(@RequestParam Long processInstanceId) {
        processInstanceService.assign(processInstanceId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/processinstances/completecurrenttask")
    public ResponseEntity completeCurrentTask(@RequestBody CompleteCurrentTaskDTO completeCurrentTaskDTO) {
        processInstanceService.completeCurrentTask(completeCurrentTaskDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
