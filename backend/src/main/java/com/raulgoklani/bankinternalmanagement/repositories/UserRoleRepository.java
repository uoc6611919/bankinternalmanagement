package com.raulgoklani.bankinternalmanagement.repositories;

import com.raulgoklani.bankinternalmanagement.models.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

}
