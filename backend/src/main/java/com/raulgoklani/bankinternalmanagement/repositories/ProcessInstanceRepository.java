package com.raulgoklani.bankinternalmanagement.repositories;

import com.raulgoklani.bankinternalmanagement.models.ProcessInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessInstanceRepository extends JpaRepository<ProcessInstance, Long> {

}
