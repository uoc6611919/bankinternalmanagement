package com.raulgoklani.bankinternalmanagement.repositories;

import com.raulgoklani.bankinternalmanagement.models.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {

}
