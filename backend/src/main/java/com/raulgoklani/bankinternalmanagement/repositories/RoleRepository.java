package com.raulgoklani.bankinternalmanagement.repositories;

import com.raulgoklani.bankinternalmanagement.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

}
