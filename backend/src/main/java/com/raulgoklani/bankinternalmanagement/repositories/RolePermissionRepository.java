package com.raulgoklani.bankinternalmanagement.repositories;

import com.raulgoklani.bankinternalmanagement.models.RolePermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolePermissionRepository extends JpaRepository<RolePermission, Long> {

}
