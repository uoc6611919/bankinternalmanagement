package com.raulgoklani.bankinternalmanagement.mappers;

import com.raulgoklani.bankinternalmanagement.dtos.CreateRoleDTO;
import com.raulgoklani.bankinternalmanagement.dtos.RoleDTO;
import com.raulgoklani.bankinternalmanagement.models.Role;
import com.raulgoklani.bankinternalmanagement.models.UserRole;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = PermissionMapper.class)
public interface RoleMapper {

    RoleDTO entityToDTO(Role role);

    @Mapping(target = "permissions", ignore = true)
    Role createDTOToEntity(CreateRoleDTO createRoleDTO);

    @Mapping(target = "id", source = "role.id")
    @Mapping(target = "name", source = "role.name")
    @Mapping(target = "permissions", source = "role.permissions")
    RoleDTO userRoleToRoleDTO(UserRole userRole);

}
