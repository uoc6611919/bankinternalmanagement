package com.raulgoklani.bankinternalmanagement.mappers;

import com.raulgoklani.bankinternalmanagement.dtos.ProcessInstanceDTO;
import com.raulgoklani.bankinternalmanagement.models.ProcessInstance;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProcessInstanceMapper {

    ProcessInstanceDTO entityToDTO(ProcessInstance processInstance);

}
