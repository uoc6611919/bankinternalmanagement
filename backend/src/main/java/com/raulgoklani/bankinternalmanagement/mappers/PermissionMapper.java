package com.raulgoklani.bankinternalmanagement.mappers;

import com.raulgoklani.bankinternalmanagement.dtos.PermissionDTO;
import com.raulgoklani.bankinternalmanagement.models.Permission;
import com.raulgoklani.bankinternalmanagement.models.RolePermission;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PermissionMapper {

    PermissionDTO entityToDTO(Permission permission);

    @Mapping(target = "id", source = "permission.id")
    @Mapping(target = "name", source = "permission.name")
    PermissionDTO rolePermissionToPermissionDTO(RolePermission rolePermission);

}
