package com.raulgoklani.bankinternalmanagement.mappers;

import com.raulgoklani.bankinternalmanagement.dtos.UserDTO;
import com.raulgoklani.bankinternalmanagement.models.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = RoleMapper.class)
public interface UserMapper {

    UserDTO entityToDTO(User user);

}
