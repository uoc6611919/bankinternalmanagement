package com.raulgoklani.bankinternalmanagement.mappers;

import com.raulgoklani.bankinternalmanagement.dtos.ProcessDefinitionDTO;
import com.raulgoklani.bankinternalmanagement.dtos.ProcessDefinitionMinimizedDTO;
import com.raulgoklani.bankinternalmanagement.models.ProcessDefinition;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProcessDefinitionMapper {

    ProcessDefinitionDTO entityToDTO(ProcessDefinition processDefinition);

    ProcessDefinitionMinimizedDTO entityToMinimizedDTO(ProcessDefinition processDefinition);

}
