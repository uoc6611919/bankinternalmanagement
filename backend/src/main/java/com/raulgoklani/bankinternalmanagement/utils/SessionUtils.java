package com.raulgoklani.bankinternalmanagement.utils;

import com.raulgoklani.bankinternalmanagement.models.User;
import org.springframework.security.core.context.SecurityContextHolder;

public class SessionUtils {

    public static User getLoggedInUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
