package com.raulgoklani.bankinternalmanagement.services;

import com.raulgoklani.bankinternalmanagement.dtos.CreateRoleDTO;
import com.raulgoklani.bankinternalmanagement.dtos.RoleDTO;
import com.raulgoklani.bankinternalmanagement.mappers.RoleMapper;
import com.raulgoklani.bankinternalmanagement.models.Permission;
import com.raulgoklani.bankinternalmanagement.models.Role;
import com.raulgoklani.bankinternalmanagement.models.RolePermission;
import com.raulgoklani.bankinternalmanagement.repositories.PermissionRepository;
import com.raulgoklani.bankinternalmanagement.repositories.RolePermissionRepository;
import com.raulgoklani.bankinternalmanagement.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RolePermissionRepository rolePermissionRepository;

    @Autowired
    private RoleMapper roleMapper;


    @Override
    public List<RoleDTO> getAll() {
        List<Role> allRoles = roleRepository.findAll();
        return allRoles.stream()
                .map(r -> roleMapper.entityToDTO(r))
                .toList();
    }

    @Override
    public RoleDTO create(CreateRoleDTO createRoleDTO) {
        Role role = createRole(createRoleDTO);
        role = createRolePermissions(createRoleDTO, role);
        return roleMapper.entityToDTO(role);
    }

    private Role createRole(CreateRoleDTO createRoleDTO) {
        Role role = roleMapper.createDTOToEntity(createRoleDTO);
        return roleRepository.save(role);
    }

    private Role createRolePermissions(CreateRoleDTO createRoleDTO, Role role) {
        List<Long> permissionIds = createRoleDTO.getPermissionIds();

        for (Long permissionId : permissionIds) {
            Permission permission = permissionRepository.findById(permissionId).get();
            RolePermission rolePermission = buildRolePermission(role, permission);
            rolePermissionRepository.save(rolePermission);

            role.getPermissions().add(rolePermission);
        }

        return roleRepository.save(role);
    }

    private RolePermission buildRolePermission(Role role, Permission permission) {
        return RolePermission.builder()
                .role(role)
                .permission(permission)
                .build();
    }

}
