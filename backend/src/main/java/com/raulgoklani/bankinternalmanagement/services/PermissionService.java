package com.raulgoklani.bankinternalmanagement.services;

import com.raulgoklani.bankinternalmanagement.dtos.PermissionDTO;

import java.util.List;

public interface PermissionService {

    List<PermissionDTO> getAll();

}
