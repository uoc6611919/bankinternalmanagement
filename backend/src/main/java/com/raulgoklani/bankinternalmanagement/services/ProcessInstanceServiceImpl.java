package com.raulgoklani.bankinternalmanagement.services;

import com.raulgoklani.bankinternalmanagement.dtos.*;
import com.raulgoklani.bankinternalmanagement.mappers.ProcessInstanceMapper;
import com.raulgoklani.bankinternalmanagement.models.*;
import com.raulgoklani.bankinternalmanagement.repositories.*;
import com.raulgoklani.bankinternalmanagement.utils.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class ProcessInstanceServiceImpl implements ProcessInstanceService {

    @Autowired
    private ProcessDefinitionRepository processDefinitionRepository;

    @Autowired
    private ProcessInstanceRepository processInstanceRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProcessInstanceMapper processInstanceMapper;


    @Override
    public List<ProcessInstanceDTO> getAll() {
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        List<ProcessInstance> allProcessInstances = processInstanceRepository.findAll(sort);
        return allProcessInstances.stream()
                .map(pi -> processInstanceMapper.entityToDTO(pi))
                .toList();
    }

    @Override
    public ProcessInstanceDTO create(CreateProcessInstanceDTO createProcessInstanceDTO) {
        ProcessInstance processInstance = buildProcessInstance(createProcessInstanceDTO);
        processInstance = processInstanceRepository.save(processInstance);
        return processInstanceMapper.entityToDTO(processInstance);
    }

    @Override
    public void assign(Long processInstanceId) {
        User loggedInUser = SessionUtils.getLoggedInUser();
        ProcessInstance processInstance = processInstanceRepository.findById(processInstanceId).get();
        User user = userRepository.findById(loggedInUser.getId()).get();
        processInstance.setUserAssigned(user);
        processInstanceRepository.save(processInstance);
    }

    @Override
    public void completeCurrentTask(CompleteCurrentTaskDTO completeCurrentTaskDTO) {
        final Long processInstanceId = completeCurrentTaskDTO.getProcessInstanceId();
        ProcessInstance processInstance = processInstanceRepository.findById(processInstanceId).get();

        CompleteCurrentTaskActionDTO action = completeCurrentTaskDTO.getAction();

        if (CompleteCurrentTaskActionDTO.CANCEL.equals(action)) {
            processInstance.setStatus(ProcessInstanceStatus.CANCELLED);
        } else if (CompleteCurrentTaskActionDTO.COMPLETE.equals(action)) {
            Optional<TaskDefinition> nextTaskOptional = getNextTask(processInstance);

            if (nextTaskOptional.isPresent()) {
                TaskDefinition nextTask = nextTaskOptional.get();
                processInstance.setCurrentTask(nextTask);
            } else {
                processInstance.setStatus(ProcessInstanceStatus.COMPLETED);
            }
        }

        processInstanceRepository.save(processInstance);
    }

    private Optional<TaskDefinition> getNextTask(ProcessInstance processInstance) {
        Long currentTaskOrder = processInstance.getCurrentTask().getOrder();
        Long nextTaskOrder = currentTaskOrder + 1L;
        return processInstance.getProcessDefinition().getTaskDefinitions().stream()
                .filter(td -> td.getOrder().equals(nextTaskOrder))
                .findFirst();
    }

    private ProcessInstance buildProcessInstance(CreateProcessInstanceDTO createProcessInstanceDTO) {
        ProcessDefinition processDefinition = processDefinitionRepository.findById(createProcessInstanceDTO.getProcessDefinitionId()).get();
        TaskDefinition currentTask = processDefinition.getTaskDefinitions().stream()
                .filter(this::isFirstTask)
                .findFirst().get();

        return ProcessInstance.builder()
                .title(createProcessInstanceDTO.getTitle())
                .status(ProcessInstanceStatus.IN_PROGRESS)
                .creationDate(LocalDateTime.now())
                .processDefinition(processDefinition)
                .currentTask(currentTask)
                .build();
    }

    private boolean isFirstTask(TaskDefinition taskDefinition) {
        return taskDefinition.getOrder().equals(1L);
    }



}
