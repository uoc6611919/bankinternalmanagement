package com.raulgoklani.bankinternalmanagement.services;

import com.raulgoklani.bankinternalmanagement.config.AuthProvider;
import com.raulgoklani.bankinternalmanagement.dtos.LinkRolesDTO;
import com.raulgoklani.bankinternalmanagement.dtos.LoginCredentialsDTO;
import com.raulgoklani.bankinternalmanagement.dtos.UserDTO;
import com.raulgoklani.bankinternalmanagement.mappers.UserMapper;
import com.raulgoklani.bankinternalmanagement.models.Role;
import com.raulgoklani.bankinternalmanagement.models.User;
import com.raulgoklani.bankinternalmanagement.models.UserRole;
import com.raulgoklani.bankinternalmanagement.repositories.RoleRepository;
import com.raulgoklani.bankinternalmanagement.repositories.UserRepository;
import com.raulgoklani.bankinternalmanagement.repositories.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private AuthProvider authProvider;

    @Override
    public UserDTO login(LoginCredentialsDTO loginCredentialsDTO) {
        String loginUsername = loginCredentialsDTO.getUsername();
        Optional<User> userOptional = userRepository.findByUsername(loginUsername);

        if (userOptional.isEmpty()) {
            throw new RuntimeException("User does not exist!");
        }

        User user = userOptional.get();
        String userPassword = user.getPassword();
        String loginPassword = loginCredentialsDTO.getPassword();

        if (!userPassword.equals(loginPassword)) {
            throw new RuntimeException("Incorrect password!");
        }

        UserDTO userDTO = userMapper.entityToDTO(user);

        String token = authProvider.createToken(userDTO.getUsername());
        userDTO.setToken(token);

        return userDTO;
    }

    @Override
    public List<UserDTO> getAll() {
        Sort sort = Sort.by("id").ascending()
                .and(Sort.by("roles.role.id").ascending())
                .and(Sort.by("roles.role.permissions.permission.id").ascending());
        List<User> allUsers = userRepository.findAll(sort);
        return allUsers.stream()
                .map(u -> userMapper.entityToDTO(u))
                .toList();
    }

    @Override
    public UserDTO linkRoles(LinkRolesDTO linkRolesDTO) {
        User user = userRepository.findById(linkRolesDTO.getUserId()).get();
        unlinkAllRoles(user);
        linkNewRoles(user, linkRolesDTO);
        user = userRepository.save(user);
        return userMapper.entityToDTO(user);
    }

    @Override
    public UserDTO findByUsername(String username) {
        User user = userRepository.findByUsername(username).get();
        return userMapper.entityToDTO(user);
    }

    private void unlinkAllRoles(User user) {
        userRoleRepository.deleteAll(user.getRoles());
        user.setRoles(new HashSet<>());
    }

    private void linkNewRoles(User user, LinkRolesDTO linkRolesDTO) {
        for (Long roleId : linkRolesDTO.getRoleIds()) {
            Role role = roleRepository.findById(roleId).get();

            UserRole userRole = UserRole.builder()
                    .user(user)
                    .role(role)
                    .build();

            userRoleRepository.save(userRole);
            user.getRoles().add(userRole);
        }
    }

}
