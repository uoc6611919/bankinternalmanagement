package com.raulgoklani.bankinternalmanagement.services;

import com.raulgoklani.bankinternalmanagement.dtos.LinkRolesDTO;
import com.raulgoklani.bankinternalmanagement.dtos.LoginCredentialsDTO;
import com.raulgoklani.bankinternalmanagement.dtos.UserDTO;

import java.util.List;

public interface UserService {

    UserDTO login(LoginCredentialsDTO loginCredentialsDTO);

    List<UserDTO> getAll();

    UserDTO linkRoles(LinkRolesDTO linkRolesDTO);

    UserDTO findByUsername(String username);

}
