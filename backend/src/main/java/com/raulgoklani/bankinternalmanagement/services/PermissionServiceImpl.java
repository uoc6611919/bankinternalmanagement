package com.raulgoklani.bankinternalmanagement.services;

import com.raulgoklani.bankinternalmanagement.dtos.PermissionDTO;
import com.raulgoklani.bankinternalmanagement.mappers.PermissionMapper;
import com.raulgoklani.bankinternalmanagement.models.Permission;
import com.raulgoklani.bankinternalmanagement.repositories.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public List<PermissionDTO> getAll() {
        List<Permission> allPermissions = permissionRepository.findAll();
        return allPermissions.stream()
                .map(u -> permissionMapper.entityToDTO(u))
                .toList();
    }

}
