package com.raulgoklani.bankinternalmanagement.services;

import com.raulgoklani.bankinternalmanagement.dtos.ProcessDefinitionMinimizedDTO;
import com.raulgoklani.bankinternalmanagement.mappers.ProcessDefinitionMapper;
import com.raulgoklani.bankinternalmanagement.models.ProcessDefinition;
import com.raulgoklani.bankinternalmanagement.repositories.ProcessDefinitionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProcessDefinitionServiceImpl implements ProcessDefinitionService {

    @Autowired
    private ProcessDefinitionRepository processDefinitionRepository;

    @Autowired
    private ProcessDefinitionMapper processDefinitionMapper;

    @Override
    public List<ProcessDefinitionMinimizedDTO> getAll() {
        List<ProcessDefinition> allProcessDefinitions = processDefinitionRepository.findAll();
        return allProcessDefinitions.stream()
                .map(pd -> processDefinitionMapper.entityToMinimizedDTO(pd))
                .toList();
    }

    @Override
    public ProcessDefinitionMinimizedDTO getById(Long id) {
        ProcessDefinition processDefinition = processDefinitionRepository.findById(id).get();
        return processDefinitionMapper.entityToMinimizedDTO(processDefinition);
    }

}
