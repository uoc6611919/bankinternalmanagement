package com.raulgoklani.bankinternalmanagement.services;

import com.raulgoklani.bankinternalmanagement.dtos.ProcessDefinitionMinimizedDTO;

import java.util.List;

public interface ProcessDefinitionService {

    List<ProcessDefinitionMinimizedDTO> getAll();

    ProcessDefinitionMinimizedDTO getById(Long id);
}
