package com.raulgoklani.bankinternalmanagement.services;

import com.raulgoklani.bankinternalmanagement.dtos.CreateRoleDTO;
import com.raulgoklani.bankinternalmanagement.dtos.RoleDTO;

import java.util.List;

public interface RoleService {

    List<RoleDTO> getAll();

    RoleDTO create(CreateRoleDTO createRoleDTO);

}
