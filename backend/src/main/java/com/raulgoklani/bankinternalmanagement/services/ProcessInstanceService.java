package com.raulgoklani.bankinternalmanagement.services;

import com.raulgoklani.bankinternalmanagement.dtos.*;

import java.util.List;

public interface ProcessInstanceService {

    List<ProcessInstanceDTO> getAll();

    ProcessInstanceDTO create(CreateProcessInstanceDTO createProcessInstanceDTO);

    void assign(Long processInstanceId);

    void completeCurrentTask(CompleteCurrentTaskDTO completeCurrentTaskDTO);
}
