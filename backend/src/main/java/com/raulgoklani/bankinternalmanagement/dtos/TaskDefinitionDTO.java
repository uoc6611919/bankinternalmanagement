package com.raulgoklani.bankinternalmanagement.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TaskDefinitionDTO {

    private Long id;
    private String name;
    private Long order;
    private PermissionDTO requiredPermission;
    private ProcessDefinitionDTO processDefinition;

}
