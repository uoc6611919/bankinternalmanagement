package com.raulgoklani.bankinternalmanagement.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProcessInstanceDTO {

    private Long id;
    private String title;
    private String status;
    private LocalDateTime creationDate;
    private UserDTO userAssigned;
    private ProcessDefinitionDTO processDefinition;
    private TaskDefinitionDTO currentTask;

}
