package com.raulgoklani.bankinternalmanagement.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProcessDefinitionMinimizedDTO {

    private Long id;
    private String name;
    private List<TaskDefinitionMinimizedDTO> taskDefinitions;

}
