package com.raulgoklani.bankinternalmanagement.dtos;

public enum CompleteCurrentTaskActionDTO {

    COMPLETE,
    CANCEL

}
