package com.raulgoklani.bankinternalmanagement.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class CompleteCurrentTaskDTO {

    final Long processInstanceId;
    final CompleteCurrentTaskActionDTO action;

}
